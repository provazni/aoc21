import pandas as pd
import string

class BinaryReading:
    def __init__(self, input_data):
        self.input_data = input_data
   
    def create_df(self):
        input_list = []
        colnum = 0
        for line in self.input_data:
            sl = [int(char) for char in line.strip()]
            colnum = len(sl)
            input_list.append(sl)
        columns = list(string.ascii_lowercase[0:colnum])
        df = pd.DataFrame(input_list, columns= columns)
        df_row_number = df.shape[0]
        return df, columns, df_row_number
    
    def get_power_consumption(self):
        gamma = []
        epsilon = []
        df, columns, df_row_number = self.create_df()
        for c in columns:
            colsum = df[c].sum()
            if colsum > (df_row_number / 2):
                gamma.append('1')
                epsilon.append('0')
            else:
                gamma.append('0')
                epsilon.append('1')
        gamma = int("".join(gamma),2)
        epsilon = int("".join(epsilon),2)
        power_consumption = gamma * epsilon
        return power_consumption

input =  open('test.txt','r')
br = BinaryReading(input)
print(f"Power Consumption is: {br.get_power_consumption()}")
