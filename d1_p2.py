input = open('input_d1_p1.txt','r').read().strip()
list =[ int(x) for x in input.split('\n')]
inc_count = 0
window_size =3

previous_window = [0,0,0]

for i in range(len(list) - window_size + 1):
    current_window = list[i: i + window_size]
    print(f"{previous_window} = {sum(previous_window)}  {current_window} = {sum(current_window)}, ")
    if sum(current_window) > sum(previous_window):
        inc_count += 1
        print(f"Increase! {inc_count}")
    previous_window = current_window

print(inc_count - 1)

