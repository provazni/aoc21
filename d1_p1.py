
input = open('input_d1_p1.txt','r')
prev_depth = ''
inc_count = 0

for current_depth in input: 
    print("Current: " + current_depth)
    print("Previous: " + prev_depth)

    if prev_depth < current_depth:
        print("Increase")
        inc_count += 1
    elif prev_depth > current_depth:
        print("Decrease")

    prev_depth = current_depth

print(inc_count)