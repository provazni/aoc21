input = open('input_d2.txt','r')
hp = 0
depth = 0
aim = 0

for line in input:
    direction, units = line.split()
    units = int(units)
    if direction == "forward" :
        hp = hp + units
        if aim > 0:
            depth = depth + (aim*units)
    elif direction == "down":
        aim = aim + units
    elif direction == "up":
        aim = aim - units

print(f"Horizontal position : {hp} \n Depth: {depth} \n Result: {hp*depth}")

