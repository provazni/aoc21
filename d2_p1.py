input = open('input_day2.txt','r')
hp = 0
depth = 0

for line in input:
    direction, units = line.split()
    units = int(units)
    if direction == "forward" :
        hp = hp + units
    elif direction == "down":
        depth = depth + units
    elif direction == "up":
        depth = depth - units

print(f"Horizontal position : {hp} \n Depth: {depth} \n Result: {hp*depth}")

